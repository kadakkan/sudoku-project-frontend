import React, { useState, useEffect } from 'react';

const CountdownTimer: React.FC = () => {
    const [seconds, setSeconds] = useState(600); // 10 minutes in seconds

    useEffect(() => {
        const interval = setInterval(() => {
            if (seconds > 0) {
                setSeconds(seconds - 1);
            }
        }, 1000);

        // Clean up the interval on unmount or if seconds is 0
        return () => clearInterval(interval);
    }, [seconds]);

    // Convert seconds to minutes and seconds for display
    const minutes = Math.floor(seconds / 60);
    const remainingSeconds = seconds % 60;

    return (
        <div className="text-center">
            <h1 className="text-3xl font-bold">Countdown Timer</h1>
            <p className="text-2xl mt-4">
                {minutes.toString().padStart(2, '0')}:{remainingSeconds.toString().padStart(2, '0')}
            </p>
        </div>
    );
};

export default CountdownTimer;
