import { Link } from "react-router-dom";
import LeaderBoard from "../components/LeaderBoard";
import Sudoku from "../components/Sudoku";
import { useAuth } from "../context/AuthContext";
import { useEffect, useState } from "react";
import { REST } from "../services/api";

const Home: React.FC = () => {
  const { user } = useAuth();
  function handleLeaderBoard() {
    REST.getLeaderboard().then((res) => {
      if (res.ok) {
        console.log(res);
        res.json().then((data) => {
          setLeaders(data.leaderboard);
        });
      }
    });
  }

  const [leaders, setLeaders] = useState<
    { username: string; score: number }[]
  >([]);

  useEffect(() => {
    REST.getLeaderboard().then((res) => {
      if (res.ok) {
        console.log(res);
        res.json().then((data) => {
          setLeaders(data.leaderboard);
        });
      }
    });
  }, []);

  return (
    <>
      <div className=" bg-gray-100 m-0 pt-10 min-h-screen ">
        <div className="container grid grid-cols-3 mx-auto w-[92%]">
          <div className="flex justify-left align-middle lg:col-span-2 col-span-3 ">
            {user ? (
              <div className="flex flex-col">
                <p className="text-3xl font-bold mb-10">
                  Logged in as {user.username}
                </p>
                <div className="items-center justify-center">
                  <Sudoku handleLeaderBoard={handleLeaderBoard} />
                </div>
              </div>
            ) : (
              <h1 className="text-4xl font-bold flex flex-col">
                Lets Play a Game of Sudoku
                <Link
                  to="/login"
                  className="w-fit px-7 py-3 mt-10 bg-purple-600 hover:bg-purple-900 text-white rounded-lg text-2xl"
                >
                  Login
                </Link>
              </h1>
            )}
          </div>
          <div className=" lg:col-span-1 col-span-3 mt-20 lg:ml-5">
            <LeaderBoard leaders={leaders} />
          </div>
        </div>
      </div>
    </>
  );
};

export default Home;
