// src/App.tsx
import React from "react";
import { BrowserRouter as Router, Route, Routes, Link } from "react-router-dom";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Register from "./pages/Register";
import { useAuth } from "./context/AuthContext";

const App: React.FC = () => {
  const { user, setUser } = useAuth();

  const handleLogout = () => {
    localStorage.removeItem("token");
    setUser(null);
  };

  return (
    <Router>
      <div>
        <nav className="bg-gray-800 shadow-lg p-4 flex justify-left items-center">
          <div className="container mx-auto flex justify-between align-middle">
            <div className="logo">
              <Link
                to="/"
                className="text-white text-2xl font-bold text-green-200"
              >
                #Sudoku
              </Link>
            </div>
            <ul className="flex flex-row gap-8">
              <li>
                <Link to="/" className="text-white">
                  Home
                </Link>
              </li>
              {!user && (
                <>
                  <li>
                    <Link to="/register" className="text-white">
                      Register
                    </Link>
                  </li>
                  <li>
                    <Link to="/login" className="text-white">
                      Login
                    </Link>
                  </li>
                </>
              )}
            </ul>
            {user && (
              <button
                onClick={handleLogout}
                className="text-white bg-red-500 px-4 py-2 rounded hover:bg-red-600 transition duration-200"
              >
                Logout
              </button>
            )}
          </div>
        </nav>

        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/register" element={<Register />} />
          <Route path="/login" element={<Login />} />
        </Routes>
      </div>
    </Router>
  );
};

export default App;
