import React from "react";

interface Props {
  puzzle: number[][];
  grid: number[][];
  handleChange: (
    row: number,
    col: number,
    e: React.ChangeEvent<HTMLInputElement>
  ) => void;
}

const Tile = ({ puzzle, grid, handleChange }: Props) => {
  return grid.map((row, rowIndex) => {
    return row.map((col, colIndex) => {
      return (
        <input
          className={
            puzzle[rowIndex][colIndex] !== 0
              ? "initial"
              : col !== 0
                ? "tile taken"
                : "tile"
          }
          value={col === 0 ? "" : col}
          key={rowIndex + " " + colIndex}
          type="text"
          onChange={(e) => handleChange(rowIndex, colIndex, e)}
        />
      );
    });
  });
};

export default Tile;
