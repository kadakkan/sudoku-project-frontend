
interface Props {
    handleInterface: (action: string) => void;
    status: string;
}

const Interface = ({ handleInterface, status }: Props) => {
  return (
    <div className="interface">
      <div className="info-interface mb-10">
        <input readOnly value={status}></input>
      </div>
      <div className="action-interface">
        <button
          className="generator-btn btn"
          onClick={() => {
            handleInterface("create");
          }}
        >
          Create
        </button>
        <button
          className="validate-btn btn"
          onClick={() => {
            handleInterface("validate");
          }}
        >
          Validate
        </button>
        <button
          className="solve-btn btn"
          onClick={() => {
            handleInterface("solve");
          }}
        >
          Solve
        </button>
        <button
          className="clear-btn btn"
          onClick={() => {
            handleInterface("clear");
          }}
        >
          Clear
        </button>
      </div>
    </div>
  );
};

export default Interface;
