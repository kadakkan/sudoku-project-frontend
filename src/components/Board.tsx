import React from "react";
import Tile from "./Tile";

interface Props {
  puzzle: number[][];
  grid: number[][];
  handleChange: (
    row: number,
    col: number,
    e: React.ChangeEvent<HTMLInputElement>
  ) => void;
}

const Board = ({ puzzle, grid, handleChange }: Props) => {
  return (
    <div className="board">
      <Tile puzzle={puzzle} grid={grid} handleChange={handleChange} />
    </div>
  );
};

export default Board;
