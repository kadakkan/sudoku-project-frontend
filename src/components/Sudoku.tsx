import { useState, useRef } from "react";
import { REST } from "../services/api";

import Board from "./Board";
import Interface from "./Interface";
import toast from "react-hot-toast";
import CountdownTimer from "../hooks/Countdown";

interface Props {
  handleLeaderBoard: () => void;
}

function getGrid() {
  const grid = [];
  for (let i = 0; i < 9; i++) {
    grid[i] = Array(9).fill(0);
  }
  return grid;
}

function copy2DArray(from: number[][], to: number[][]) {
  for (let i = 0; i < from.length; i++) {
    to[i] = [...from[i]];
  }
}

const Sudoku = ({ handleLeaderBoard }: Props) => {
  const [grid, setGrid] = useState(getGrid);
  const [puzzleStatus, setPuzzleStatus] = useState("** UNSOLVED **");
  const initialGrid = useRef(getGrid());

  function handleChange(
    row: number,
    col: number,
    e: React.ChangeEvent<HTMLInputElement>
  ) {
    const re = /^[0-9\b]+$/;
    if (e.target.value === "" || re.test(e.target.value)) {
      if (Number(e.target.value) < 10 && initialGrid.current[row][col] === 0) {
        const newGrid = [...grid];
        newGrid[row][col] = Number(e.target.value);
        setGrid(newGrid);
      }
    }
  }

  async function handleInterface(action: string) {
    let newGrid;
    switch (action) {
      case "create": {
        newGrid = await handleCreate();
        copy2DArray(newGrid, initialGrid.current);
        setPuzzleStatus("");
        setGrid(newGrid);
        break;
      }
      case "solve": {
        newGrid = await handleSolve();
        setGrid(newGrid);
        break;
      }
      case "clear": {
        newGrid = getGrid();
        copy2DArray(newGrid, initialGrid.current);
        setGrid(newGrid);
        setPuzzleStatus("");
        break;
      }
      case "validate": {
        const status = await handleValidate();
        const puzzStats = status ? "** SOLVED **" : "** UNSOLVED **";
        if (status) {
          toast.success("Puzzle validation successful!");
          handleLeaderBoard();
        } else {
          toast.error("Puzzle validation failed!");
        }

        setPuzzleStatus(puzzStats);
        break;
      }
      default: {
        throw new Error("Invalid action");
      }
    }
  }

  async function handleCreate() {
    try {
      const response = await REST.getBoard();
      const data = await response.json();
      return data.game;
    } catch (error) {
      console.log(error);
    }
  }

  async function handleValidate() {
    try {
      const response = await REST.validateBoard(grid);
      const data = await response.json();
      return data.status;
    } catch (error) {
      console.log(error);
    }
  }

  async function handleSolve() {
    try {
      const response = await REST.solveBoard(grid);
      const data = await response.json();
      if (data.status) {
        setPuzzleStatus("** SOLVED **");
        toast.success("Puzzle solved successfully!");
        return data.solution;
      } else {
        toast("Puzzle is unsolvable!");

        setPuzzleStatus("** UNSOLVABLE **");
        return grid;
      }
    } catch (error) {
      console.log(error);
    }
  }

  return (
    <div className="grid grid-col grid-cols-3 items-center justify-center bg-slate-300 px-5 py-2 min-h-36 rounded-lg">
      <div className="flex flex-wrap lg:col-span-2 col-span-3 justify-center">
        <Board
          puzzle={initialGrid.current}
          grid={grid}
          handleChange={handleChange}
        />
      </div>
      <div className="flex flex-wrap align-middle justify-center items-center space-y-10 lg:col-span-1 col-span-3 mb-10">
        <div className="text-4xl text-gray-600">
          <CountdownTimer />
        </div>
        <Interface handleInterface={handleInterface} status={puzzleStatus} />
      </div>
    </div>
  );
};

export default Sudoku;
