interface Props {
  leaders: { username: string; score: number }[];
}

const LeaderBoard = ({ leaders}: Props) => {
  // const leaders = [
  //   { name: "John Doe", score: 100 },
  //   { name: "Jane Doe", score: 200 },
  //   { name: "Alice", score: 300 },
  // ];

  return (
    <>
      <div className="flex flex-col p-4  bg-white rounded-lg">
        <h3 className="text-xl font-bold mb-4">Leaderboard</h3>
        <ol className="space-y-2">
          {leaders.map((leader, index) => (
            <li key={index} className="flex justify-between items-center">
              <span className="font-semibold">
                {index + 1}. {leader.username}
              </span>
              <span className="text-gray-600">Score: {leader.score}</span>
            </li>
          ))}
        </ol>
      </div>
    </>
  );
};

export default LeaderBoard;
