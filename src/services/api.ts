import { API_URL } from "../config";

export const REST = {
    getBoard: function () {
      return fetch(`${API_URL}/api/puzzle`);
    },
    solveBoard: function (grid: number[][]) {
      const data = {
        board: grid,
      };
      return fetch(`${API_URL}/api/solve`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "Authorization": `${localStorage.getItem("token")}`,
        },
        body: JSON.stringify(data), 
      });
    },
    validateBoard: function (grid: number[][]) {
      const data = {
        board: grid,
      };
      return fetch(`${API_URL}/api/validate`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "Authorization": `${localStorage.getItem("token")}`,
        },
        body: JSON.stringify(data),
      });
    },
    getLeaderboard: function () {
      return fetch(`${API_URL}/api/leaderBoard`);
    }
  };